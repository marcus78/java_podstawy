package downhill;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class Man extends Ghost implements Data {

    private int dx;

    public Man() {
        initMan();
    }
    private void initMan() {
        loadImage();
        getImageDimensions();
        resetMan();
    }
    private void loadImage() {
        ImageIcon ii = new ImageIcon("C:\\IdeaProjects\\11_listopada2018ND\\out\\production\\11_listopada2018ND\\alx\\src\\main\\resources\\man.png");
        image = ii.getImage();
    }
    public void move() {
        x += dx;
        if (x <= 0) { // left
            x = 0;
        }
        if (x >= WIDTH - imageWidth) { // right
            x = WIDTH - imageWidth;
        }
    }
    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            dx = -1;
        }
        if (key == KeyEvent.VK_RIGHT) {
            dx = 1;
        }
    }
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }
        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }
    }
    private void resetMan() {
        x = MAN_X;
        y = MAN_Y;
    }
}