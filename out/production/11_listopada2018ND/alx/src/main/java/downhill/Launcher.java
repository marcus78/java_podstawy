package downhill;

import javax.swing.*;

public class Launcher extends JFrame {

    public Launcher() {

        initUI();
    }
    private void initUI() {
        add(new Board());
        setTitle("Downhill");

        int a = 800;
        int b = 440;

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(a, b);
    }
    public static void main(String[] args) {

        Launcher game = new Launcher();
            game.setVisible(true);
    }
}