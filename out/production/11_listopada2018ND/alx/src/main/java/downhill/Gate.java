package downhill;

import javax.swing.*;

public class Gate extends Ghost implements Data {

    private int xdir;
    private int ydir;

    public Gate() {
        initGate();
    }
    private void initGate() {
        loadImage();
        getImageDimensions();
        resetState();
    }
    private void loadImage() {
        ImageIcon ii = new ImageIcon("C:\\IdeaProjects\\11_listopada2018ND\\out\\production\\11_listopada2018ND\\alx\\src\\main\\resources\\gate.png");
        image = ii.getImage();
    }
    public void move() {
        y += ydir;
        if (x == 0) {
            setXDir(1);
        }
        if (x == WIDTH - imageWidth) {
            setXDir(-1);
        }
        if (y == 0) {
            setYDir(1);
        }
    }
    public void resetState() {
        double r;
        r = Math.random() * 250;
        x = (int) r;
        y = INIT_BALL_Y;
    }
    public void setXDir(int x) {
        xdir = x;
    }
    public void setYDir(int y) {
        ydir = y;
    }

}