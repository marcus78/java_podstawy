package downhill;

public interface Data {

    public static final int WIDTH = 300;
    public static final int BOTTOM_EDGE = 390;
    public static final int MAN_X = 180;
    public static final int MAN_Y = 360;
    public static final int INIT_BALL_Y = 0;

}
