package downhill;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;

public class Board extends JPanel implements Data {

    private Timer timer;
    private Gate gate;
    private Man man;
    private boolean inGame = true;
    private int z;

    public Board() {
        initBoard();
    }
    private void initBoard() {
        addKeyListener(new TAdapter());
        setFocusable(true);

        timer = new Timer();
        timer.scheduleAtFixedRate(new ScheduleTask(), 550, 7);
    }
    @Override
    public void addNotify() {
        super.addNotify();
        gameInit();
    }
    private void gameInit() {
        gate = new Gate();
        man = new Man();
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        if (inGame) {
            drawObjects(g2d);
        } else {
            gameFinished(g2d);
        }
        Toolkit.getDefaultToolkit().sync();
    }
    private void drawObjects(Graphics2D g2d) {

        g2d.drawImage(gate.getImage(), gate.getX(), gate.getY(),
                gate.getImageWidth(), gate.getImageHeight(), this);
        g2d.drawImage(man.getImage(), man.getX(), man.getY(),
                man.getImageWidth(), man.getImageHeight(), this);

        Font font = new Font("Verdana", Font.BOLD, 18);
        FontMetrics metr = this.getFontMetrics(font);

        g2d.setColor(Color.BLACK);
        g2d.setFont(font);
        g2d.drawString("Score: " + String.valueOf(String.valueOf(z)),
                600,
                Data.WIDTH / 2);
    }

    private void gameFinished(Graphics2D g2d) {
        Font font = new Font("Verdana", Font.BOLD, 18);
        FontMetrics metr = this.getFontMetrics(font);

        g2d.setColor(Color.BLACK);
        g2d.setFont(font);
        g2d.drawString("Score :" + String.valueOf(String.valueOf(z)),
                600,
                Data.WIDTH / 2);

        g2d.setColor(Color.BLACK);
        g2d.setFont(font);
        g2d.drawString("Game over",
                400,
                Data.WIDTH / 2);
    }
    private class TAdapter extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            man.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            man.keyPressed(e);
        }
    }
    private class ScheduleTask extends TimerTask {
        @Override
        public void run() {
            gate.move();
            man.move();
            checkCollision();
            repaint();
        }
    }
    private void stopGame() {

        inGame = false;
        timer.cancel();
        //addNotify();

    }

    public void checkCollision() {

        if (gate.getRect().getMaxY() > Data.BOTTOM_EDGE) {

            //stopGame();
            addNotify();
        }

        if ((gate.getRect()).intersects(man.getRect())) {

            int paddleLPos = (int) man.getRect().getMinX();
            int ballLPos = (int) gate.getRect().getMinX();

            int first = paddleLPos;

            if (ballLPos < first + 150) {
                z = z + 1;
                gate.setXDir(-1);
                gate.setYDir(-1);
                gate.resetState();
            }
            gate.setXDir(1);
            gate.setYDir(1);
        }
    }
}
