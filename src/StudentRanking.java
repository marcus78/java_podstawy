//          Napisz program, który wypisze na ekran konsoli,
//        słowo oznaczające ocenę dla podanej cyfry.
//        Np. dla 1 - "niedostateczny", 2 - "mierny" itp.
//        Obsłuż przypadek gdy cyfra jest poza skalą ocen.

import java.util.Scanner;

public class StudentRanking {
    public static void main(String[] args) {

        int rating;  // Ocena studenta
        Scanner reading = new Scanner(System.in);
        System.out.println("Proszę podać ocenę :");
        rating = reading.nextInt();
        switch (rating) {
            case 1:
                System.out.println("Ocena niedostateczna");
                break;
            case 2:
                System.out.println("Ocena mierna");
                break;
            case 3:
                System.out.println("Ocena dostateczna");
                break;
            case 4:
                System.out.println("Ocena dobra");
                break;
            case 5:
                System.out.println("Ocena bardzo dobra");
                break;
            case 6:
                System.out.println("Ocena celująca");
                break;
            default:
                System.out.println("Out of scale");
        }

    }
}
