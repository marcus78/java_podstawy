public class Slv1{

    private int[] firsts = {65, 1488, 3840};
    private String[] alphabets =  {"Latin", "Hebrew", "Tibetan"};

    public int[] getFirsts() {
        return firsts;
    }

    public String[] getAlphabets() {
        return alphabets;
    }
}