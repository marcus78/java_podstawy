//Napisz program, który wypisze na ekran konsoli, cyfrę arabską dla podanej liczby rzymskiej (od 1 do 9).
//        Czyli np. dla 'I' wypisze 1, dla 'V' 5 itp. Obsłuż przypadek gdy podana liczba rzymska jest nieprawidłowa.

import java.util.Scanner;

public class Roman2Arab {
    public static void main(String[] args) {

        String arabSign;
        Scanner reader = new Scanner(System.in);
        System.out.println("Podaj liczbę w systemie rzymskim :");
        arabSign = reader.next();

        switch (arabSign) {
            case "I":
                System.out.println("Liczba 1");
                break;
            case "II":
                System.out.println("Liczba 2");
                break;
            case "III":
                System.out.println("Liczba 3");
                break;
            case "VI":
                System.out.println("Liczba 4");
                break;
            case "V":
                System.out.println("Liczba 5");
                break;
            default:
                System.out.println("Liczba nieobsługiwana");

        }
    }

}
