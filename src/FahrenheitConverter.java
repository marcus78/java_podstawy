public class FahrenheitConverter {

    public static void main(String[] args) {

        double farenheit = 32.0;
        double celsius = 100.0;
        double wynikFarenheit;
        double wynikCelsius;

        wynikFarenheit = 32+(9.0/5.0)*celsius;
        wynikCelsius = 5.0/9.0*(farenheit - 32);

        System.out.println("Dla "+farenheit+" Farenheitow, wynik w st C to: "+wynikCelsius);
        System.out.println("Dla "+celsius+" st. Celsjusza, wynik w st F to: "+wynikFarenheit);
    }
}
