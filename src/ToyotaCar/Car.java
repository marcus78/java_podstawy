//          Utwórz klasę Car z polami: brand i color
//        {na katalogu}(Alt + Insert → Java Class lub PPM → New →  Java Class)

package ToyotaCar;

public class Car {

    String color;
    String brand;

    public Car(String color, String brand) {
        this.color = color;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
