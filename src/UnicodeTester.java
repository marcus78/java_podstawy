//          Napisz program, który wypisze na ekran konsoli czy podany kod Unicode jest liczbą (0-9),
//        małą literą (a-z) czy też dużą literą (A-Z). Kody każdej z grup
//        znaków następują po sobie więc wystarczy znaleźć kod
//        np. dla litery 'a' i 'z' i sprawdzić czy podany kod zawiera się w tym przedziale.

import java.util.Scanner;

public class UnicodeTester {
    public static void main(String[] args) {

        String sign;
        Scanner reader = new Scanner(System.in);
        System.out.println("Podaj znak : ");
        sign = reader.next();

        //String sign ="@";
        char ptak = sign.charAt(0);
        int asciiNum = (int) ptak;
        // System.out.println(asciiNum);

        if (asciiNum >= 48 && asciiNum <= 57) {
            System.out.println("Liczba z przedziału 0-9");
        }
        if (asciiNum >= 65 && asciiNum <= 90) {
            System.out.println("Litera z przedziału A-Z");
        }
        if (asciiNum >= 97 && asciiNum <= 122) {
            System.out.println("Litera z przedziału a-z");
        }
    }
}
