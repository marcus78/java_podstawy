package Money;

public enum Operation {

    SUM("+"), MINUS("-"), MULTI("*"), DIV("/");

    String action;

    Operation(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public int dodawanie(int a, int b) {

        return a + b;
    }

    public int odejmowanie(int a, int b) {
        return a - b;
    }

    public String calculate(String nazwa) {
        switch (nazwa) {
            case "+":
                System.out.println("Robimy dodawanie");
                System.out.println(dodawanie(1, 2));
                break;
            case "-":
                System.out.println("Odejmujemy !");
                System.out.println(odejmowanie(5, 8));
        }
        return "";
    }
}