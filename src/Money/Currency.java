package Money;

public enum Currency {
    ZLOTY("pln"),DOLAR("usd"),EURO("eur"),YEN("yen"),FUNT("gbp");

    String nazwa;

    Currency(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }
}