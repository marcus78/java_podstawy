package School;

public class TestFamily {

    public static void main(String[] args) {
        Person fatherNowak = new Person("Jan","Nowak",49);
        Person motherNowak = new Person("Anna","Nowak",39);
        Person sonNowak = new Person("Mark","Nowak",19);
        Person daughterNowak = new Person("Zuza","Nowak",11);


        Person fatherLewy = new Person("Bob","Lewy",49);
        Person motherLewy = new Person("Dzesika","Lewy",42);
        Person sonLewy = new Person("Robert","Lewy",9);
        Person daughterLewy = new Person("Sonia","Lewy",12);


        Family nowaki = new Family(fatherNowak, motherNowak, sonNowak, daughterNowak);
        Family lewaki = new Family(fatherLewy,motherLewy, sonLewy, daughterLewy);

        //System.out.println(fatherNowak.getName()+fatherNowak.getSurname()+fatherNowak.getAge());
        System.out.println(nowaki.getFamilyDescription());
        System.out.println(lewaki.getFamilyDescription());
        System.out.println("");
        System.out.println("Nowak`s age: "+ nowaki.getSumAgeFamily());
        System.out.println("Lewi`s age: "+ lewaki.getSumAgeFamily());
        System.out.println("");
        System.out.println("Nowak`s average: "+ nowaki.getAverageAgeFamily());
        System.out.println("Lewi`s average: "+ lewaki.getAverageAgeFamily());


    }
}
