package School;

public class Family {

    Person father;
    Person mother;
    Person son;
    Person daughter;

    public Family(Person father, Person mother, Person son, Person daughter) {
        this.father = father;
        this.mother = mother;
        this.son = son;
        this.daughter = daughter;
    }

    public Person getFather() {
        return father;
    }

    public Person getMother() {
        return mother;
    }

    public Person getDaughter() {
        return daughter;
    }

    public Person getSon() {
        return son;

    }

    public String getFamilyDescription() {
        return getFather().name + " " + getFather().surname+" "+getFather().age+" "+
                getMother().name + " "+ getMother().surname+" "+getMother().age+" "+
                getDaughter().name+" "+ getDaughter().surname+" "+ getDaughter().age+" "+
                getSon().name+" "+ getSon().surname+" "+ getSon().age;
    }
    public int getSumAgeFamily(){
        return getFather().age + getMother().age + getSon().age + getDaughter().age;
    }
    public double getAverageAgeFamily(){
        return getSumAgeFamily()/4;
    }
}
