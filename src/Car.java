//          Utwórz klasę Car z polami: brand i color
//        {na katalogu}(Alt + Insert → Java Class lub PPM → New →  Java Class)

public class Car {

    String brand;
    String color;

    public Car(String brand, String color) {
        this.brand = brand;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
