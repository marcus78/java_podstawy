
package TemperatureUnits;

import static java.lang.Math.round;

public class FarenheitConverter {
    public static void main(String[] args) {

        Temperature test = new Temperature();
        System.out.println(test.showInCelsius("11 lipiec","12:32", 88.2));
        System.out.println(test.showInFarenheit("12 maj","14:21",212));

    }
}
