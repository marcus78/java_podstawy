package TemperatureUnits;

import static java.lang.Math.round;

public class Temperature {

    double farenheit;
    double celsius;

    public double convertFarenheitToCelsius (double farenheit) {
        this.celsius = celsius;
        return round(5.0/9.0*(farenheit - 32));
    }
    public double convertCelsiusToFarenheit(double celsius){
        this.farenheit = farenheit;
        return round(32+(9.0/5.0)*celsius);
    }

    public String showInCelsius(String date, String hour, double farenheit){
        return date +" "+ hour +" "+ convertFarenheitToCelsius(farenheit)+" C";
    }
    public String showInFarenheit(String date, String hour, double celsius){
        return date +" "+ hour +" "+ convertCelsiusToFarenheit(celsius)+" F";
    }
}