package aleksandria;

import aleksandria.Areable;

public enum BookCategory implements Areable {

    Adventure, Comedy, Drama, Horror, Poem, Romntic, SciFi;
    public BookCategory category;

    public BookCategory getCategory() {
        return category;
    }

}
