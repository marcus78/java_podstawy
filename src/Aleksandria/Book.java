package Aleksandria;

public class Book {

    private String title;
    private String author;
    private String shortDescription;
    private int nomberOfPages;
public Book(){}
    public Book(String title, String author, String shortDescription, int nomberOfPages) {
        this.title = title;
        this.author = author;
        this.shortDescription = shortDescription;
        this.nomberOfPages = nomberOfPages;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public int getNomberOfPages() {
        return nomberOfPages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", nomberOfPages=" + nomberOfPages +
                '}';
    }
}

//    private String autor;
//    private String tytul;
//    private String opis;
//
//    public Book(String autor, String tytul, String opis) {
//        this.autor = autor;
//        this.tytul = tytul;
//        this.opis = opis;
//    }
//    public Book(){}
//
//    public String getAutor() {
//        return autor;
//    }
//
//    public void setAutor(String autor) {
//        this.autor = autor;
//    }
//
//    public String getTytul() {
//        return tytul;
//    }
//
//    public void setTytul(String tytul) {
//        this.tytul = tytul;
//    }
//
//    public String getOpis() {
//        return opis;
//    }
//
//    public void setOpis(String opis) {
//        this.opis = opis;
//    }
//
//    @Override
//    public String toString() {
//        return "Book{" +
//                "autor='" + autor + '\'' +
//                ", tytul='" + tytul + '\'' +
//                ", opis='" + opis + '\'' +
//                '}';
//    }