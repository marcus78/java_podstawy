package aleksandria;

import Aleksandria.Book;
import aleksandria.enums.BookCategory;
import aleksandria.enums.VinylCategory;

import java.util.ArrayList;

public class AlexandiaLibrary {

    public static void main(String[] args) {
        ArrayList<Item> stuff = new ArrayList<>();
        stuff.add(new Book("Moonraker", "Pikaczu", "O stworkach", 12, BookCategory.Adventure));
        stuff.add(new Vinyl("Kopacze", "Hopsasa", "Lalala", VinylCategory.Funk));

        System.out.println(stuff.get(1).getAuthor() + " " + stuff.get(0).getCategory() + " " +
                stuff.get(1).getCategory());

        for (int i = 0; i < stuff.size(); i++) {

            Item item = stuff.get(i);

            if (item instanceof Vinyl) {

                Vinyl vinyl = (Vinyl) item;

                System.out.println(stuff.get(i).getAuthor() + " " + stuff.get(i).getDescription());

                System.out.println("vinyl.getCategory() = " + vinyl.getCategory());

            }

            if (item instanceof Book) {

                Book book = (Book) item;

                System.out.println(stuff.get(i).getAuthor() + " " + stuff.get(i).getDescription() + " "+
                        stuff.get(i).getNumberOfPages());

                System.out.println("book.getCategory() = " + book.getCategory());

            }
        }
    }
}