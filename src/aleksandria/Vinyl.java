package aleksandria;

import aleksandria.enums.VinylCategory;

public class Vinyl extends Item {

    public VinylCategory category;


    public Vinyl(String title, String author, String description, VinylCategory category) {
        super(title, author, description);
        this.category = category;
    }

    public VinylCategory getCategory() {
        return category;
    }
}
