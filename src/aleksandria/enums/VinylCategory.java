package aleksandria.enums;

import aleksandria.Areable;
import aleksandria.Item;

public enum VinylCategory implements Areable{
    Folk, Funk, Pop, Rock;

    public VinylCategory category;

    public VinylCategory getCategory(){
        return category;
    }
}
