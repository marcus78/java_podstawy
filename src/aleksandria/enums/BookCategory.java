package aleksandria.enums;

import aleksandria.Areable;
import aleksandria.Item;

public enum BookCategory implements Areable{
    Adventure, Drama, Poem, SciFi;

    public BookCategory category;

    public BookCategory getCategory() {
        return category;
    }
}
