package Aleksandria;

import aleksandria.Item;
import aleksandria.enums.BookCategory;

public class Book extends Item {

    private String title;
    private String author;
    private String shortDescription;
    private int numberOfPages;
public Book(String moonraker, String pikaczu, String o_stworkach, int i, BookCategory adventure){}
    public Book(String title, String author, String shortDescription, int numberOfPages) {
        this.title = title;
        this.author = author;
        this.shortDescription = shortDescription;
        this.numberOfPages = numberOfPages;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }


}

//    private String autor;
//    private String tytul;
//    private String opis;
//
//    public Book(String autor, String tytul, String opis) {
//        this.autor = autor;
//        this.tytul = tytul;
//        this.opis = opis;
//    }
//    public Book(){}
//
//    public String getAutor() {
//        return autor;
//    }
//
//    public void setAutor(String autor) {
//        this.autor = autor;
//    }
//
//    public String getTytul() {
//        return tytul;
//    }
//
//    public void setTytul(String tytul) {
//        this.tytul = tytul;
//    }
//
//    public String getOpis() {
//        return opis;
//    }
//
//    public void setOpis(String opis) {
//        this.opis = opis;
//    }
//
//    @Override
//    public String toString() {
//        return "Book{" +
//                "autor='" + autor + '\'' +
//                ", tytul='" + tytul + '\'' +
//                ", opis='" + opis + '\'' +
//                '}';
//    }