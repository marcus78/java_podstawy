package aleksandria;

public class Item{

    private String title;
    private String author;
    private String description;
    private Item category;


    public Item(String title, String author, String description) {
        this.title = title;
        this.author = author;
        this.description = description;
    }

    public Item() {
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public int getNumberOfPages() {
        return getNumberOfPages();
    }

    public Areable getCategory() {
        return (Areable) category;
    }

}
