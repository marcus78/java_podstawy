//        Napisz program, który wypisze na ekran konsoli, czy dana liczba całkowita znajduje się w przedziale
//        1-10,
//        11-100,
//        101-1000,
//        1001-10000,
//        czy też może jest mniejsza od 0 lub większa od 10000.
//        Parametrem wejściowym niech będzie zmienna zainicjowana na początku programu.

import java.util.Scanner;

public class NumericIntervals {
    public static void main(String[] args) {

        int nuberTested;
        Scanner reading = new Scanner(System.in);
        nuberTested = reading.nextInt();

        if (nuberTested >= 1 && nuberTested <= 10) {

            System.out.println("Podales  liczbe w przedziale 1-10 ");
        }
        if (nuberTested >= 11 && nuberTested <= 100) {

            System.out.println("Podales  liczbe w przedziale 11-100 ");
        }

        if (nuberTested >= 101 && nuberTested <= 1000) {

            System.out.println("Podales  liczbe w przedziale 101-1000 ");
        }
        if (nuberTested >= 1001 && nuberTested <= 10000) {

            System.out.println("Podales  liczbe w przedziale 1001-10000 ");
        }

        else {
            System.out.println("Podales innaczej");
        }
    }
}
