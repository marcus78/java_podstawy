//Napisz program, który dla podanej liczby wypisze na ekran konsoli dzień tygodnia
//        (dla 1 - "poniedziałek", 2 - "wtorek" itp). Dodatkowo wyświetl ile dni zostało do weekendu,
//        dla poniedziałku - 5 dni, wtorku - 4 itp.


import java.util.Scanner;

public class WeekControl {
    public static void main(String[] args) {
        int dayNumber;
        Scanner reader = new Scanner(System.in);
        System.out.println("Podaj numer dnia tygodnia 1-7");
        dayNumber = reader.nextInt();
        switch (dayNumber) {
            case 1:
                System.out.println("Monday");
                System.out.println((6 - dayNumber) + " days remain until the weekend.");
                break;
            case 2:
                System.out.println("Tuesday");
                System.out.println((6 - dayNumber) + " days remain until the weekend.");
                break;
            case 3:
                System.out.println("Wednesday");
                System.out.println((6 - dayNumber) + " days remain until the weekend.");
                break;
            case 4:
                System.out.println("Thursday");
                System.out.println((6 - dayNumber) + " days remain until the weekend.");
                break;
            case 5:
                System.out.println("Friday");
                System.out.println((6 - dayNumber) + " days remain until the weekend.");
                break;
            case 6:
                System.out.println("Saturday");
                System.out.println((6 - dayNumber) + " days remain until the weekend. WEEKEND !");
                break;
            case 7:
                System.out.println("Sunday");
                System.out.println((7 - dayNumber) + " days remain until the weekend. WEEKEND !");
                break;
            default:
                System.out.println("Undefined situation");
                break;

        }
    }
}
