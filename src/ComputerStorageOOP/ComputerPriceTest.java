package ComputerStorageOOP;

public class ComputerPriceTest {

    public static void main(String[] args) {

         ComputerPrice test = new ComputerPrice(10,20,30,40,50);

        System.out.println("Computer price (VAT)= "+ test.getComputerPrice());
        System.out.println("Monitor price (VAT) = " + test.getMonitorPrice());
        System.out.println("Computer with monitor price (VAT) = " + test.getComputerAndMonitorPrice());

    }

}
