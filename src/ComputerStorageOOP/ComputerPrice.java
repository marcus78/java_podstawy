//          W klasie ComputerPrice wydziel metody getComputerPrice(), getMonitorPrice() i getComputerAndMonitorPrice(),
//        ostatnia z metod ma korzystać z dwóch pierwszych. Zmienną VAT ustaw jako pole klasy ComputerPrice.

package ComputerStorageOOP;

public class ComputerPrice {

    private double mainboardPrice;
    private double processorPrice;
    private double memoryRamPrice;
    private double monitorPrice;
    private double hddPrice;
    private final double taxValue = 1.23;

    public ComputerPrice(double mainboardPrice, double processorPrice, double memoryRamPrice,
                         double monitorPrice, double hddPrice) {
        this.mainboardPrice = mainboardPrice;
        this.processorPrice = processorPrice;
        this.memoryRamPrice = memoryRamPrice;
        this.monitorPrice = monitorPrice;
        this.hddPrice = hddPrice;
    }

    public double getComputerAndMonitorPrice() {
        return getComputerPrice() + getMonitorPrice();
    }

    public double getComputerPrice() {
        return (mainboardPrice + processorPrice + memoryRamPrice + hddPrice) * taxValue;
    }

    public double getMonitorPrice() {
        return monitorPrice * taxValue;
    }
}


